# Cloning repository #

> you have to download/clone repo

## Context ##
To start working on the project you'll need to download the repository code

## Steps ##
1. navigate to the right directory
    - good to have short path to our repository. Examples: _/c/repositories_, _C:/repositories_ 
    - use `cd` command to navigate in directories structure
2. copy <git-repo-url> from the repository. The most popular syntaxes of <git-repo-url>: 
    - http[s]://host.xz[:port]/path/to/repo.git/
    - ssh://[user@]host.xz[:port]/path/to/repo.git/
3. clone repo from <git-repo-url> (Directory <git-repo-name> will be created automatically)
4. make sure that the download was successful

## Example ##

Example, type `git --version` to make sure that you have installed Git:
```
git version 2.10.1.windows.1
```

###### Assumptions: ######
 - I have installed Git
 - I am in the right directory
 - I have access to the repository that I want to download

### Console ###
1) To download the source code from repository, type:
```
git clone <git-repo-url>
```
2) To check that you've downloaded the repository, type:
```
cd repoDir
git status
```

After successful download you should see similar output:
```
$ git clone <git-repo-url>
Cloning into 'repoName'...
remote: Counting objects: 18, done.
remote: Compressing objects: 100% (17/17), done.
remote: Total 18 (delta 6), reused 0 (delta 0)
Unpacking objects: 100% (18/18), done.
```

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
```

### SourceTree ###