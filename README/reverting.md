#  Reverting changes #

> revert committed changes

## Focus ##
This use case is devoted in reverting changes by adding new commit

## Steps ##
1. identify changes to be reverted (commit-id)
2. revert changes
3. update commit message
4. make be sure if reverting is finished

## Example ##

Example, type `git log` to see commits history:
```
commit 2b3715893afe2f4b45fe959f2ac10d13640a3f26 (HEAD -> feature/feature1)
Author: Murphy, Alice  <Alice.Murphy@domain.com>
Date:   Mon Feb 5 09:26:58 2018 +0100

    add empty line

```

###### Assumptions: ######
- I am on `feature/feature1` branch
- I have commit with id 2b3715893afe2f4b45fe959f2ac10d13640a3f26

### Console ###

1. To check last commit, type
```
git log --max-count=1
```
2. To revert changes, type:
```
git revert 2b3715893afe2f4b45fe959f2ac10d13640a3f26
```
3. Save commit message
4. To check commits history, type:
```
git log --max-count=2
```

After successful revert you should see similar output:
```
commit 6584d9ad4886f02b3624fb8f85361037d0d7bccf (HEAD -> feature/feature1)
Author: Murphy, Alice <Alice.Murphy@domain.com>
Date:   Mon Feb 5 09:32:47 2018 +0100

    Revert "add empty line"

    This reverts commit 2b3715893afe2f4b45fe959f2ac10d13640a3f26.

commit 2b3715893afe2f4b45fe959f2ac10d13640a3f26
Author: Murphy, Alice <Alice.Murphy@domain.com>
Date:   Mon Feb 5 09:26:58 2018 +0100

    add empty line

```

### SourceTree ###