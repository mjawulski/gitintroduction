#  Conflicts #

> you have unmerged paths.

## Focus ##
The conflicts occur when we want to edit file which someone has changed the same line of a file or when file was deleted and these changes are pushed on remote before us. The conflicts occur after merge operation.

## Steps ##
1. identify files containing conflicts
2. open files in favourite text editor
3. identify differences between HEAD and branch
4. decide what to keep (HEAD, branch of combination of them)
5. save changes
6. you can commit changes in normal mode (add,commit)

repeat these steps for all conflicted files

## Example ##

Example, type `git status` when you have conflicts:
```
# On branch feature/feature1
# You have unmerged paths.
#   (fix conflicts and run "git commit")
#
# Unmerged paths:
#   (use "git add ..." to mark resolution)
#
# both modified:      README.md
#
no changes added to commit (use "git add" and/or "git commit -a")
```

###### Assumptions: ######
- I am on `feature/feature1` branch
- I have conflicts on `README.md` file

### Console ###

1. To check which files contain conflicts, type:
```
git status
```
2. After opening file you can see:
```
It is my first README file
<<<<<<< HEAD
powered by admin
=======
powered by user
>>>>>>> feature/feature1
```
3. Identify differences between HEAD and branch
```
It is my first README file  - unchanged part of code
<<<<<<< HEAD
powered by admin            - changes from remote
=======
powered by user
>>>>>>> feature1            - change from my branch
```
4. Decide to keep (in this case combine versions)
```
It is my first README file
powered by admin and user
```
5. Save changes
6. To commit all changes, type:
```
git add
git commit -m "change committer credentials"
git push origin feature1
```

### SourceTree ###